package com.snapdeal.pogService.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snapdeal.pogService.db.pogDb;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class pogService {

	@Autowired
    private pogDb pogDb;

	public String getDsc(String pogId) {

		String desc = null;
		desc = pogDb.getPogDesc(Integer.parseInt(pogId));
		
		return desc;
	}

	public ArrayList<String> getPcodes(String pogId) {
		// TODO Auto-generated method stub
		
		ArrayList<String> supcs = pogDb.getSupcs(Integer.parseInt(pogId));

		return supcs;
	}

	public ArrayList<String> getPnames(String pogId) {
		// TODO Auto-generated method stub
		
		ArrayList<String> temp = pogDb.getPnames(Integer.parseInt(pogId));
	
		return temp;
	
	}
    
	public String addPog(String desc){
		return pogDb.addPog(desc);	
	}
	
	public String addProd(String pogId, String supc, String name){
		return pogDb.addProd(Integer.parseInt(pogId),supc,name);
	}
	
	public String updPog(String pogId, String name){
		return pogDb.updatePog(Integer.parseInt(pogId),name);
	}
	
	public String updProd(String supcId, String name){
		return pogDb.updateProd(supcId,name);
	}
	
	public String delPog(String pogId){
		return pogDb.delPog(Integer.parseInt(pogId));
	}
	
	public String delProd(String supcId){
		return pogDb.delProd(supcId);
	}
}
