package com.snapdeal.pogService.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.awt.List;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;

import com.snapdeal.pogService.beans.addPogResponse;
import com.snapdeal.pogService.beans.addProdResponse;
import com.snapdeal.pogService.beans.delPogResponse;
import com.snapdeal.pogService.beans.delProdResponse;
import com.snapdeal.pogService.beans.pogRequest;
import com.snapdeal.pogService.beans.pogResponse;
import com.snapdeal.pogService.beans.updPogResponse;
import com.snapdeal.pogService.beans.updProdResponse;
import com.snapdeal.pogService.service.pogService;
import com.snapdeal.pogService.controller.*;
@Controller
public class pogController {

	 @Autowired
	    private pogService pogService;
	 
	    @RequestMapping("/getValuesJson")
	    @ResponseBody
	    public pogResponse getResponse2(@RequestBody pogRequest request) {
	       
	        ArrayList<String> name = pogService.getPnames(request.getpogId());
			String desc = pogService.getDsc(request.getpogId());
			ArrayList<String> supcs = pogService.getPcodes(request.getpogId());
			pogResponse response = new pogResponse(request.getpogId(), name, desc, supcs);
			
	        return response;
	    }
	 @RequestMapping("/getValues")
		@ResponseBody
		public pogResponse getResponse(@RequestParam(required = true, value = "pogId") String pogId) {
			ArrayList<String> name = pogService.getPnames(pogId);
			String desc = pogService.getDsc(pogId);
			ArrayList<String> supcs = pogService.getPcodes(pogId);
			pogResponse response = new pogResponse(pogId, name, desc, supcs);
			return response;
		}
	 
	 @RequestMapping("/addPog")
		@ResponseBody
		public addPogResponse getAddPogResponse( @RequestParam(required = true, value = "dsc") String desc) {
			String flag= pogService.addPog(desc);
			addPogResponse response = new addPogResponse(flag);
			return response;
		}
	 
	 @RequestMapping("/addProd")
		@ResponseBody
		public addProdResponse getAddProdResponse(@RequestParam(required = true, value = "pogid") String pogId, @RequestParam(required = true, value = "supcid") String supc ,@RequestParam(required = true, value = "prodname") String name) {
			String flag=pogService.addProd(pogId, supc,name);
			addProdResponse response = new addProdResponse(flag);
			return response;
		}
	 
	 @RequestMapping("/updPog")
		@ResponseBody
		public updPogResponse getUpdPogResponse(@RequestParam(required = true, value = "pogid") String pogId,@RequestParam(required = true, value = "dsc") String name) {
			String flag=pogService.updPog(pogId, name);
			updPogResponse response = new updPogResponse(flag);
			return response;
		}
	 
	 @RequestMapping("/updProd")
		@ResponseBody
		public updProdResponse getUpdProdResponse(@RequestParam(required = true, value = "supcid") String supcId,@RequestParam(required = true, value = "prodname") String name) {
			String flag=pogService.updProd(supcId, name);
			updProdResponse response = new updProdResponse(flag);
			return response;
		}
	 
	 @RequestMapping("/delProd")
		@ResponseBody
		public delProdResponse getdelProdResponse(@RequestParam(required = true, value = "supcid") String supcId) {
			String flag=pogService.delProd(supcId);
			delProdResponse response = new delProdResponse(flag);
			return response;
		}
	 
	 @RequestMapping("/delPog")
		@ResponseBody
		public delPogResponse getdelPogResponse(@RequestParam(required = true, value = "pogid") String pogId ) {
			String flag=pogService.delPog(pogId);
		    delPogResponse response = new delPogResponse(flag);
			return response;
		}
	 
	 
	 
	 @RequestMapping("/displayResult")	  
	 public ModelAndView calculate(@RequestParam(required = true, value = "pogId") String pogId) {
	        
		    ModelAndView modelAndView = new ModelAndView();
	        String dsc = pogService.getDsc(pogId);
	       
	        
	        ArrayList<String> supc = new ArrayList<String>();
	        supc= pogService.getPcodes(pogId);
	        
	        String spc="";
	       for(int i=0;i<supc.size();i++){
	    	   spc= spc+supc.get(i).toString()+",";
	       }
	        
	      
	        modelAndView.addObject("pogId", pogId);
	        modelAndView.addObject("dsc", dsc);
	        modelAndView.addObject("spc",supc);
	        
	        
	        
	        modelAndView.setViewName("details");
	        
	        return modelAndView;
	    }
	
}
