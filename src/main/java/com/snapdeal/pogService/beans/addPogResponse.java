package com.snapdeal.pogService.beans;

public class addPogResponse {
	
	private String msg;
	
	public addPogResponse(String msg){
		super();
		 
		   if(msg!=null)
			this.msg=msg;
		   else
			this.msg="Request Successfull !!";
	}
	
	public String getMsg(){
		return msg;
	}
	
	public void setMsg(String msg){
		this.msg=msg;
	}
}