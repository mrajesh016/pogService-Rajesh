package com.snapdeal.pogService.beans;

public class addProdResponse {
	
private String msg;
	
	public addProdResponse(String msg){
		   super();
		   
		   if(msg!=null)
			this.msg=msg;
		   else
			this.msg="Request Successfull !!";
	}
	
	public String getMsg(){
		return msg;
	}
	
	public void setMsg(String msg){
		this.msg=msg;
	}
}