package com.snapdeal.pogService.beans;

import java.util.ArrayList;

public class pogRequest {

   
	private String pogId;
	
    public String getpogId() {
        return pogId;
    }

    public void setpogId(String pogId) {
        this.pogId = pogId;
    }


    @Override
    public String toString() {
        return String.format("pogRequest [pogId=%s]", pogId);
    }
}