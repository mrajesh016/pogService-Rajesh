package com.snapdeal.pogService.beans;

public class updProdResponse {
	
	private String msg;
	
	public updProdResponse(String msg){
		super();
		 
		   if(msg!=null)
			this.msg=msg;
		   else
			this.msg="Request Successfull !!";
	}
	
	public String getMsg(){
		return msg;
	}
	
	public void setMsg(String msg){
		this.msg=msg;
	}
}