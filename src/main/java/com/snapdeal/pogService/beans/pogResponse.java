package com.snapdeal.pogService.beans;

import java.util.ArrayList;

public class pogResponse {
	
	

    private String dsc ;
    private ArrayList<String> supcs;
    private ArrayList<String> pnames;
	private String pogId;
    
    public pogResponse() {
    }

    public pogResponse(String pogId,ArrayList<String> pnames, String dsc,ArrayList<String> supc) {
    	super();
        this.pogId=pogId;
    	this.dsc = dsc;
        this.supcs = supc;
        this.pnames = pnames;
    }


    public String getpogId() {
        return pogId;
    }

    public void setpogId(String pogId) {
        this.pogId = pogId;
    }
    
    public String getDsc() {
        return dsc;
    }

    public void setDsc(String dsc) {
        this.dsc = dsc;
    }

    public ArrayList<String> getSupc() {
        return supcs;
    }

    public void setSupc(ArrayList<String> supc) {
        this.supcs = supc;
    }

    public ArrayList<String> getPnames() {
        return pnames;
    }

    public void setPnames(ArrayList<String> pnames) {
        this.pnames = pnames;
    }
}
