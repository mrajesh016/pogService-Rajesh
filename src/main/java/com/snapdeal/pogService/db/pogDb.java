package com.snapdeal.pogService.db;


import java.awt.List;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.sym.Name;

@Component("pogDb")
public class pogDb {
	private static String JDBC_DRIVER = "com.mysql.jdbc.Driver";
    private static String DB_URL      = "jdbc:mysql://127.0.0.1:3306/pog_db";

    //  Database credentials
    private static String USER        = "root";
    private static String PASS        = "mrajesh016";
    
    public String getPogDesc(int pogId) {
        try {
            Class.forName(JDBC_DRIVER);
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement ps = con.prepareStatement("select description from pog where id = ?");
            ps.setInt(1, pogId);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                String name = rs.getString(1);
                return name;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return null;
    }
    
   
    public ArrayList<String> getPnames(int pogId) {
        try {
            Class.forName(JDBC_DRIVER);
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement ps = con.prepareStatement("select name from pog_supc where pog_id = ?");
            ps.setInt(1, pogId);
            ResultSet rs = ps.executeQuery();
            ArrayList<String> names = new ArrayList<String>();
            while (rs.next()) {
                names.add(rs.getString(1));
            }
            return names;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public ArrayList<String> getSupcs(int pogId) {
        try {
            Class.forName(JDBC_DRIVER);
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
            PreparedStatement ps = con.prepareStatement("select supc_id from pog_supc where pog_id = ?");
            ps.setInt(1, pogId);
            ResultSet rs = ps.executeQuery();
            ArrayList<String> names = new ArrayList<String>();
            while (rs.next()) {
                names.add(rs.getString(1));
            }
            return names;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public String addPog(String desc) {
    	String ecode=null;
        try {
            Class.forName(JDBC_DRIVER);
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
            Statement stmt = null;
            stmt = con.createStatement();
            String sql="INSERT INTO pog (description) values('"+desc+"')";
            //ps.setInt(1, pogId);
            stmt.executeUpdate(sql);
        } catch (Exception e) {
        	StringWriter sw = new StringWriter();
        	e.printStackTrace(new PrintWriter(sw));
            ecode=sw.toString();
            //return null;
        }
        return ecode;
    }
    
    public String addProd(int pogId, String supc, String name) {
    	String ecode=null;
          try {
            Class.forName(JDBC_DRIVER);
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
            
            Statement st=con.createStatement();
            
            String ps = "select * from pog where id ="+pogId;
           
            ResultSet rs = st.executeQuery(ps);
           
            int cnt=0;
            while (rs.next()) {
            	cnt++;
                
            }
            
            if(cnt!=0){
            	 Statement stmt = null;
                 
            	 stmt = con.createStatement();
                 
            String sql="Insert into pog_supc (pog_id,supc_id,name) values("+pogId+", '"+supc+"' , '"+name+"')";
            stmt.executeUpdate(sql);}
           
            
            if(cnt==0)
         	   ecode="The enetered pogId is INVALID !!";
            
        } catch (Exception e) {
        	StringWriter sw = new StringWriter();
        	e.printStackTrace(new PrintWriter(sw));
            ecode="The entered pogId doesn't exist !!";
            //return null;
        }
        return ecode;
    }
       public String updatePog(int pogId, String name) {
    		String ecode=null;
    	    
        try {
            Class.forName(JDBC_DRIVER);
            Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
             PreparedStatement ps = con.prepareStatement("select * from pog where id = ?");
            ps.setInt(1, pogId);
            ResultSet rs = ps.executeQuery();
            ArrayList<String> names = new ArrayList<String>();
            int cnt=0;
            while (rs.next()) {
            	cnt++;
                names.add(rs.getString(1));
            }
            Statement stmt=con.createStatement();
            stmt.executeUpdate("Update pog set description= '"+name+"' where id="+pogId);
            
            if (cnt==0)
            	ecode="The entered pogId doesn't exist !!";
        } 
        catch (Exception e) {
        	StringWriter sw = new StringWriter();
        	e.printStackTrace(new PrintWriter(sw));
            ecode="The entered pogId doesn't exist !!";
            //return null;
        }
        return ecode;
    }
       public String updateProd(String supcId, String name) {
   		String ecode=null;
   	    
       try {
           Class.forName(JDBC_DRIVER);
           Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
           
           Statement st=con.createStatement();
           
           String ps = "select * from pog_supc where supc_id ='"+supcId+"'";
          
           ResultSet rs = st.executeQuery(ps);
          
           int cnt=0;
           while (rs.next()) {
           	cnt++;
               
           }
           
           Statement stmt=con.createStatement();      
           stmt.executeUpdate("Update pog_supc set name= '"+name+"' where supc_id='"+supcId+"'");
           
           if(cnt==0)
        	   ecode="The enetered supcId doesn't exist !!";
       } 
       catch (Exception e) {
       	StringWriter sw = new StringWriter();
       	e.printStackTrace(new PrintWriter(sw));
           ecode="The enetered supcId doesn't exist !!";
           //return null;
       }
       return ecode;
   }
       public String delPog(int pogId) {
     		String ecode=null;
     	    
         try {
             Class.forName(JDBC_DRIVER);
             Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
             PreparedStatement ps = con.prepareStatement("select * from pog where id = ?");
             ps.setInt(1, pogId);
             ResultSet rs = ps.executeQuery();
             ArrayList<String> names = new ArrayList<String>();
             int cnt=0;
             while (rs.next()) {
             	cnt++;
                 names.add(rs.getString(1));
             }
             Statement stmt=con.createStatement();
              stmt.executeUpdate("DELETE FROM pog where id="+pogId);
              
              
              if(cnt==0)
           	   ecode="The enetered pogId doesn't exist !!";
              
         } 
         catch (Exception e) {
         	StringWriter sw = new StringWriter();
         	e.printStackTrace(new PrintWriter(sw));
             ecode="The enetered pogId doesn't exist !!";
             //return null;
         }
         return ecode;
     }
         
       public String delProd(String supcId) {
      		String ecode=null;
      	    
          try {
              Class.forName(JDBC_DRIVER);
              Connection con = DriverManager.getConnection(DB_URL, USER, PASS);
              Statement st=con.createStatement();
              
              String ps = "select * from pog_supc where supc_id = '"+supcId+"'";
             
              ResultSet rs = st.executeQuery(ps);
             
              int cnt=0;
              while (rs.next()) {
              	cnt++;
                  
              }
              
              Statement stmt=con.createStatement();
              
            stmt.executeUpdate("DELETE FROM pog_supc where supc_id='"+supcId+"'");
            
            if(cnt==0)
         	   ecode="The enetered supcId doesn't exist !!";
            
          } 
          catch (Exception e) {
          	StringWriter sw = new StringWriter();
          	e.printStackTrace(new PrintWriter(sw));
              ecode="The enetered supcId doesn't exist !!";
              //return null;
          }
          return ecode;
      }
          
}